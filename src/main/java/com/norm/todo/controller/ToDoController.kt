package com.norm.todo.controller

import com.norm.todo.model.ToDo
import com.norm.todo.repository.ToDoRepository
import org.springframework.web.bind.annotation.*

@RestController
class ToDoController(private val todoRepository: ToDoRepository) {

    @GetMapping(value = ["/api/todo"])
    fun getToDoList(): List<ToDo> {
        return todoRepository.findAll()
    }

    @GetMapping("/api/todo/{id}")
    fun getToDo(@PathVariable("id") todoId: String): ToDo {
        return todoRepository.findById(todoId).get()
    }

    @PostMapping(value = ["/api/todo"])
    fun saveToDo(@RequestBody todo: ToDo): ToDo {
        return todoRepository.save(todo)
    }

    @DeleteMapping(value = ["/api/todo/{id}"])
    fun deleteToDo(@PathVariable("id") todoId: String) {
        todoRepository.deleteById(todoId)
    }
}
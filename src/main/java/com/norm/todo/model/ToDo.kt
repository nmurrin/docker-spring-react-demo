package com.norm.todo.model

import org.springframework.data.annotation.Id

data class ToDo (
        @Id
        val id: String?,
        var task: String,
        var description: String)
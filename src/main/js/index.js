import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import promise from 'redux-promise';

import Todos from './components/todos';
import TodosNew from './components/todos_new';
import TodosShow from './components/todos_show';
import reducers from './reducers';
import {createRoot} from "react-dom/client";

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

const container = document.querySelector('.container-fluid');

const root = createRoot(container);

root.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
            <div>
                <Routes>
                    <Route path="/todos/new" element={<TodosNew />} />
                    <Route path="/todos/:id" element={<TodosShow />} />
                    <Route path="/" element={<Todos />} />
                </Routes>
            </div>
        </BrowserRouter>
    </Provider>
);

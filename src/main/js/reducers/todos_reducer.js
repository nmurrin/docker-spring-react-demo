import { FETCH_TODO, FETCH_TODOS, DELETE_TODO } from '../actions';

export default function (state = {}, action) {
    switch (action.type) {
        case DELETE_TODO:
            return _.omit(state, action.payload);
        case FETCH_TODO:
            return {...state, [action.payload.data.id]: action.payload.data };
        case FETCH_TODOS:
            console.log("Reducing...", action);
            return { ...state, ['list']: action.payload.data };
        default:
            return state;
    }
}

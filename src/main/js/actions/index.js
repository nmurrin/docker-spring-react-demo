import axios from 'axios';

export const FETCH_TODOS = 'fetch_todos';
export const FETCH_TODO = 'fetch_todo';
export const CREATE_TODO = 'create_todo';
export const DELETE_TODO = 'delete_todo';

const ROOT_URL = '/api';

export function fetchTodos() {
    const request = axios.get(`${ROOT_URL}/todo`);

    return {
        type: FETCH_TODOS,
        payload: request
    };
}

export function createTodo(values, callback) {
    const request = axios.post(`${ROOT_URL}/todo`, values)
        .then(() => { callback(); });

    return {
        type: CREATE_TODO,
        payload: request
    };
}

export function fetchTodo(id) {
    const request = axios.get(`${ROOT_URL}/todo/${id}`);

    return {
        type: FETCH_TODO,
        payload: request
    };
}

export function deleteTodo(id, callback) {
    const request = axios.delete(`${ROOT_URL}/todo/${id}`)
        .then(() => { callback(); });

    return {
        type: DELETE_TODO,
        payload: id
    };
}
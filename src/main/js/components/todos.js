import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import {fetchTodos} from '../actions';

const Todos = () => {

    const todos = useSelector((state) => state.todos.list);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchTodos());
    }, [dispatch]);

    const renderTodos = () => {
        console.log(todos);
        return todos.map((todo) => {
            return (
                <tr key={todo.id}>
                    <td><Link to={`/todos/${todo.id}`}>{todo.task}</Link></td>
                    <td>{todo.description}</td>
                </tr>
            )
        })
    }

    return (
        <>

            {!todos &&
                <div>Loading...</div>
            }

            {todos &&
                <>
                    <div className="text-xs-right">
                        <Link className="btn btn-primary" to="/todos/new">
                            Add a Todo
                        </Link>
                    </div>

                    <h3>Todo List</h3>

                    <table className="table table-striped table-hover">
                        <thead className="thead-dark">
                        <tr>
                            <th>Task</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        {renderTodos()}
                        </tbody>
                    </table>
                </>
            }

        </>
    );

}

export default Todos;
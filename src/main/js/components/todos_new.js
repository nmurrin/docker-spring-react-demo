import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { createTodo } from '../actions';
import { useForm } from "react-hook-form";

const TodosNew = () => {

    const { register, handleSubmit, formState: { errors } } = useForm();
    const dispatch = useDispatch();
    let navigate = useNavigate();

    const onSubmit = (values) => {
        dispatch(createTodo(values, () => {
            navigate('/');
        }))
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className={`form-group ${errors.task ? 'has-danger' : ''}`}>
                <label>Task</label>
                <input
                    {...register('task', {required: true})}
                    className="form-control"
                    type="text"
                />
                <div className="text-help">
                    {errors.task ? 'Please enter a task name' : ''}
                </div>
            </div>

            <div className={`form-group ${errors.descroption ? 'has-danger' : ''}`}>
                <label>Description</label>
                <input
                    {...register('description', {required: true})}
                    className="form-control"
                    type="text"
                />
                <div className="text-help">
                    {errors.description ? 'Please enter a task description' : ''}
                </div>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
            <Link to="/" className="btn btn-danger">Cancel</Link>

        </form>
    );
}

export default TodosNew;
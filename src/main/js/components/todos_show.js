import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { deleteTodo, fetchTodo } from '../actions';

const TodosShow = () => {
    const dispatch = useDispatch();
    let navigate = useNavigate();
    const { id } = useParams();
    const todo = useSelector((state) => state.todos[id]);

    useEffect(() => {
        dispatch(fetchTodo(id));
    }, [dispatch]);

    const onDeleteClick = () => {
        dispatch(deleteTodo(id, () => {
            navigate('/');
        }))
    }

    return (
        <>
            {!todo &&
                <div>Loading...</div>
            }

            {todo &&
                <div>
                    <Link to="/">Back to Index</Link>
                    <button
                        className="btn btn-danger pull-xs-right"
                        onClick={onDeleteClick}>
                        Delete Todo
                    </button>
                    <h3>{todo.task}</h3>
                    <h6>Description:</h6>
                    <p>{todo.description}</p>
                </div>
            }
        </>
    )
}

export default TodosShow;
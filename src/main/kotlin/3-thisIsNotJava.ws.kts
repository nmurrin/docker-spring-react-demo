// Named params, default values
fun foo(b: String = "My Number is: ", a: Int = 0, ) = println(b + a)
foo(a = 1)

// List declaration and ranges
val myNumbers = listOf(1, 2, 3)
val moreNumbers = 1..100

// Filter a list
val myOddNumber = moreNumbers.filter { it % 2 == 1 }
println(myOddNumber)

// Ranges
println((1..100 step 2).toList())
println((99 downTo 1 step 2).toList())

// Map declaration
val myMap = mutableMapOf("a" to 1, "b" to 2, "c" to 3)
println(myMap["b"])
myMap["d"] = 4

// Check for presence
println("Is a in map: ${"a" in myMap}")
println("Is e in map: ${"e" in myMap}")

// Creating a singleton
object Resource {
    val name = "Name"
}
println(Resource.name)

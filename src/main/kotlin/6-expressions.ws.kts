
val a = 10
val b = 20
// if / else is an expression and can return a value
val x = if (a == b) 30 else 40
println(x)

val c: String? = null
// try / catch is a expression and can return a value
// Have you had to initialize a var outside of try catch in Java?
val y = try {
    c!!.length
} catch (x: Exception) {
    0
}
println("Length is: $y")


var x: String? = null

// Does not compile!
//println(x.length)
println(x?.length)
println(x?.length ?: "empty")

x = "Hello"
// Still does not compile
//println(x.length)
println(x!!.length)

val emails = mutableListOf<String>()
// Execute if not null
emails.firstOrNull()?.let {
    println("Sending email to $it")
}

emails.add("norman.murrin@hertz.com")
// Execute if not null
emails.firstOrNull()?.let {
    println("Sending email to $it")
}






// Function basics, type inference
fun sum(a: Int, b: Int): Int {
    return a + b
}
sum(7, 5)
fun sum2(a: Int, b: Int) = a + b
sum2(7, 5)

// Variable basics
val a: Int = 1
var b = 2
var c: Int? = null
b = b++

// Strings
val s = "Hello\nworld!"
println(s)

val text = """Hello
    |World!
""".trimMargin()
println(text)

println("$s.length is ${s.length}")





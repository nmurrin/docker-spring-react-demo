# kotlin-demo
To run the Spring application will need:
- docker
- java 17
- node 18

# Run local mongo server (needed for application)
# -d = daemon mode (run in background)
docker pull mongo
docker run --name mongo -d -p 27017:27017 mongo

# Run the npm install locally to load javascript modules
npm install

# Package javascript files into Java project
./gradlew bundleJs

# Run the java project
./gradlew bootRun

# Build and run using a Dockerfile
# Not great
# - Uses a large base image
# - takes a long time to launch!
docker network create mynetwork
docker build -f ./Dockerfile1 -t norm/todo .
docker run --name mongo -d --net mynetwork -p 27017:27017 mongo
docker run --name todo-app -d --net mynetwork -p 8080:8080 norm/todo

# Build locally, run using a Dockerfile
# Better
# - Runs really quick!
# - I need a complete and correctly configred build environment to build
npm run bundle
./gradlew jar
docker build -f ./Dockerfile2 -t norm/todo .
docker run --name todo-app -d --net mynetwork -p 8080:8080 norm/todo

# Use a multi-stage Dockerfile
docker build -f ./Dockerfile2 -t norm/todo .
docker run --name todo-app -d --net mynetwork -p 8080:8080 norm/todo

# Use docker compose to run
docker compose up
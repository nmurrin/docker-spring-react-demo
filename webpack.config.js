const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/main/js/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'src', 'main', 'resources', 'static'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devServer: {
    historyApiFallback: true,
    static: './',
    port: 3000
  },
  devtool: 'inline-source-map'
};
